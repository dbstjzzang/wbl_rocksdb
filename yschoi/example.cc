#include "rocksdb/db.h"
using namespace ROCKSDB_NAMESPACE;
int main()
{
	DB* db;
	Options options;
	options.create_if_missing = true;
	Status s = DB::Open(options, "/tmp/testdb", &db);
	assert(s.ok());

	s = db->Put(WriteOptions(), "key1", "value");
	assert(s.ok());
	std::string value;
	s = db->Get(rocksdb::ReadOptions(), "key1", &value);
	assert(s.ok());
	delete db;

	return 0;
}
